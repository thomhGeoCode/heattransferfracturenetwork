# HeatTransferFractureNetwork

## This is an exemplary implementation of using the upscaled version of heat transfer in a fractured network.

The version was tested using Matlab R2023b. With the current setting the runtime was around 2 hours on an Intel i7-8700.

## License
Along with this file and the source code you should have received a version of our license.

## Copyright (c) 2024 Thomas Heinze
This file was written by Thomas Heinze, Bochum, Germany, 2024