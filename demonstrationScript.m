%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2024, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2024
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all
clc

%% user input data
% spatial resolution (m)
dx = 50;
% Chosen time step (s)
dt = 0.5;

% inflow temperature (°C)
T1 = 60;
% initial rock temperature (°C)
Tc = 140;

% Distance of injection/production well (m)
L = 750.0;
% Width of fracture = thickness of fractured layer (m)
D = 200.0;

% Fracture spacing (m)
B = 5;
% Mean fracture aperture (m)
apert = 3.0e-5;
% Total flow rate (m^3/s)
FlowRate = 40.0*1e-3;

% Duration of simulation in years
durationYrs = 30;

%% Parameters for rock and fluid
% Thermal conductivity of rock (W/m/°C)
kR = 2.6;
% Heat capacity of rock (J/kg/°C)
cpR = 790;
% Rock density (kg/m^3)
rhoR = 2650;
% Water density (kg/m^3)
rhoW = 1000.0;
% Heat capacity of water (J/kg/°C)
cpW = 4184;
% Thermal conductivity of water (W/m/°C)
tCondW = 0.6;
% Viscosity of water (Pa s)
Visc = 3.2e-4;

%% Parameters calculated based on user input
secondsInYear = 365.0 * 24.0 * 60.0 * 60.0;

% Number of fractures (-)
nrFrac = D/B;

% duration of simulation in (s)
duration = durationYrs * secondsInYear;

% Thermal diffusivity to speed up calculation (m^2/s)
kappaR = kR / rhoR / cpR;

% time counter
time = 0.0;
% counter for year for output
year = 0.0;
% counter for saving once per year
i=1;

% Flow velocity in fractures (m/s)
vel = FlowRate / (D*apert) / nrFrac;

% Reynolds Nr (-)
Re = rhoW * vel * apert / Visc;
% Prandtl Nr (-)
Pr = cpW * Visc / tCondW;
% Nusselt Nr (-)
Nu = sqrt(Pr * Re);
% Heat transfer coefficient (W/m^2/K)
h = Nu / (B*0.5) * tCondW;

% number of grid points
nx = int16(L/dx)+1;

% Initialize arrays
Velocity = vel .* ones(1,nx);
TempF = Tc.* ones(1,nx);
TempR = Tc.* ones(1,nx);
Tout = zeros(1,durationYrs+1);
Tout(1) = Tc;

%% Start simulation
while time < duration

  % 1d diffusion in the rock
  TempR = diffusionRock1d(TempR, kappaR, dt, dx);
  % Boundary conditions
  TempR(1) = TempR(2);
  TempR(end)= TempR(end-1);

  % 1d advection in the fluid
  TempF = advectionUpwind1stOrder1d(TempF, Velocity, dt, dx);
  % Boundary conditions
  TempF(1) = T1;
  TempF(end) = TempF(end-1);
  
  % heat transfer
  Q = 2.0 * h * (TempF - TempR);
  TempR = TempR + dt/(rhoR * cpR) .* Q ./ B;
  TempF = TempF - dt/(rhoW * cpW) .* Q ./ apert;

  time = time + dt;
  year = year + dt;
  if year >= secondsInYear
    year = 0.0;
    i=i+1;
    Tout(i) = TempF(end);
    if (isnan(Tout(i)))
      disp('Abort isnan(Tout)')
      break
    end
    if (Tout(i) > Tc)
      disp('Abort Tout>Tc')
      break
    end
  end
end

%% Visualization of results
figure()
plot(0:1:durationYrs, Tout)
xlabel('time (yrs)')
ylabel('production temperature (°C)')

%%
function NewValue = diffusionRock1d(Value, kappa, dt, dz)

  NewValue = Value;
  NewValue(2:end-1) = Value(2:end-1) + dt .* kappa .* 
    (Value(1:end-2) - 2.0 .* Value(2:end-1) + Value(3:end)) ./ (dz*dz);

end

%%
function NewValue = advectionUpwind1stOrder1d(Value, Vel, dt, dz)

  NewValue = Value;
  NewValue(2:end) = Value(2:end) - dt .* Vel(2:end) .* ...
    ( Value(2:end) - Value(1:end-1) ) ./dz;

end